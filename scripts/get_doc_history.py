#!/usr/bin/env python3

import argparse
import html5lib
import json
import os
import re
import sys

def getTable(dom, id):
	for t in dom.getElementsByTagName('table'):
		if t.hasAttribute('id') and t.getAttribute('id') == id:
			return t
	return None

LINKREGEX = re.compile('<a href="\\?docid=([0-9]+)">(.+)</a>', re.I)
def parse_link(link):
    if link == '---':
        return None, None
    m = LINKREGEX.fullmatch(link)
    assert m, link
    return tuple(x for x in m.groups())

def get_evolution_info(docpath):
    with open(docpath) as f:
        dom = html5lib.parse(f, treebuilder="dom")
        changers = getTable(dom, 'Changers')
        for t in changers.getElementsByTagName('table'):
            row = t.getElementsByTagName('td')
            changerLink = row[0].firstChild.toxml()
            revLink     = row[1].firstChild.toxml()
            yield parse_link(changerLink) + parse_link(revLink)


def doc_history_entry(ch_docid, ch_docname, rev_doc_id):
    return dict(ch_docid=ch_docid, ch_docname=ch_docname, rev_doc_id=rev_doc_id)

def get_doc_history(doc_file_path, docid):
    print(f'get_doc_history({doc_file_path}, {docid})', file=sys.stderr)
    doc_evolution_info = tuple(get_evolution_info(doc_file_path))
    doc_names = set(x[3] for x in doc_evolution_info)
    if None in doc_names:
        doc_names.remove(None)
    assert len(doc_names) == 1, doc_names
    doc_name = tuple(doc_names)[0]

    doc_history = [doc_history_entry(docid, doc_name, docid)]
    for c in doc_evolution_info:
        doc_history.append(doc_history_entry(c[0], c[1], c[2]))

    return dict(name=doc_name, history=doc_history)

def get_history_of_multiple_docs(docdir, docids):
    for docid in docids:
        doc_file_path = f"{docdir}/DocumentView.aspx?docid={docid}"
        yield get_doc_history(doc_file_path, docid)

mydir = os.path.dirname(__file__)
datadir = os.path.relpath(mydir + "/../data")

parser = argparse.ArgumentParser()
parser.add_argument('docid', nargs='+')
args = parser.parse_args()

r = get_history_of_multiple_docs(f"{datadir}/www.arlis.am", args.docid)

json.dump(tuple(r), sys.stdout, indent=2, ensure_ascii=False)
