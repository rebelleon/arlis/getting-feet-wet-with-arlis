#!/usr/bin/env bash

mydir=$(dirname "$0")
cd "$mydir"/../data

cat doc_info.json|jq '[.[].DocType]|unique' > doctypes.json
cat doc_info.json|jq '[.[].ActType]|unique' > acttypes.json
cat doc_info.json|jq '[.[].ActStatus]|unique' > actstatuses.json

cat doc_info.json|jq 'map(select([.DocType == "Պաշտոնական Ինկորպորացիա", .ActStatus == "Գործում է", ([.ActType == "Օրենսգիրք", .ActType == "Օրենք"]|any)]|all))' > active_laws_with_revisions.json

cat active_laws_with_revisions.json | jq '[.[] | {orig_doc_id, Title}]|unique' > active_laws_orig_only.json
cat active_laws_orig_only.json | jq '[.[].orig_doc_id]|unique' > active_laws_orig_doc_ids.json

docids=($(jq '.[]' active_laws_orig_doc_ids.json|sed 's/"//g'|sort -n))

../scripts/get_doc_history.py "${docids[@]}" > active_laws_history.json
