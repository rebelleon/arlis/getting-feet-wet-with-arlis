#!/usr/bin/env python3

import argparse
import glob
import json
import re
import sys

parser = argparse.ArgumentParser()
parser.add_argument('glob')
args = parser.parse_args()

paths = glob.glob(args.glob)


DIVIDREGEX = re.compile('<DIV id=([^>]+)>(.+)</DIV></TD>.*', re.I)

ORIG_DOC_REF_LINE_PREFIX = '<TD class=diz vAlign=top colSpan=3 align=center>Մայր փաստաթուղթ:<A href="?'

def getOrigDocId(line):
    line = line.removeprefix(ORIG_DOC_REF_LINE_PREFIX)
    line = line.split('"')[0]
    return line.split('=')[1]

TRANSLATIONLANGREGEX = re.compile("Annexes/LNG_button_([^.]+).")
TRANSLATIONREFREGEX = re.compile('<A href="\?docid=([^"]+)"', re.I)
def getDocTranslationInfo(links):
    t = []
    for s in links.split('</A> ')[:-1]:
        lang = TRANSLATIONLANGREGEX.search(s).groups()[0]
        docid = TRANSLATIONREFREGEX.search(s).groups()[0]
        t.append(dict(lang=lang, docid=docid))

    return "translations", t

def getDocAttribute(pseudoName, pseudoValue):
    if pseudoName.startswith('langSwitches'):
        return getDocTranslationInfo(pseudoValue)

    return pseudoName, pseudoValue

def getDocFields(path):
    result = { "orig_doc_id": None }
    with open(path, 'r') as f:
        for line in f:
            if 'div id=' in line.lower():
                m = DIVIDREGEX.match(line)
                if m is not None:
                    attrName, attrValue = getDocAttribute(m[1], m[2])
                    result[attrName]= attrValue
            elif line.startswith(ORIG_DOC_REF_LINE_PREFIX):
                result["orig_doc_id"] = getOrigDocId(line)

    return result


def process(path):
    result = dict(path=path)
    result.update(getDocFields(path))
    return result

a = []
n = len(paths)
for i, p in enumerate(paths):
    print(f"[{i}/{n}] {p}", file=sys.stderr)
    a.append(process(p))

json.dump(a, sys.stdout, indent=2, ensure_ascii=False)
