#!/usr/bin/env bash

mydir=$(dirname "$0")
cd "$mydir"/..

docfileglob='DocumentView.aspx[?][Dd][Oo][Cc][Ii][Dd]=*'
scripts/process_docs.py data/www.arlis.am/"$docfileglob" > data/doc_info.json
